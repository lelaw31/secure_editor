"use strict";

// TODO: add a control of correct decipher
//       add error message display
function loadFile(inputFile) {
  askForPass("Decrypt", function(key){loadNdecrypt(inputFile, key);});
}
function loadNdecrypt(inputFile, key) {
  if (inputFile.files.length == 0)
    return;
  var fileName = inputFile.files[0].name;
  var fr = new FileReader();
  fr.onload = function(e) {
    const data = new Uint8Array(e.target.result);
    document.getElementById('editor').value = decipher(data, key);
    document.getElementById('saveLink').download = fileName;
  };
  fr.readAsArrayBuffer(inputFile.files[0]);
  inputFile.value = '';
}
function saveFile() {
  askForPass("Encrypt", encryptNsave);
}
function encryptNsave(key) {
  const text = document.getElementById('editor').value;
  const bb = new Blob([cipher(text, key)], {type : 'application/octet-stream'});
  var a = document.getElementById('saveLink');
  a.href = window.URL.createObjectURL(bb);
  a.click();
  window.URL.revokeObjectURL(a.href);
}
function newFile() {
  document.getElementById('editor').value = '';
}
function askForPass(operation, submitFunc) {
  var processFunc = function() {
    hidePassPanel();
    if (document.querySelector('input[name="keyType"]:checked').value == "key") {
      const keyStr = document.getElementById('key').value;
      if (keyStr.length != 64 || keyStr.match(/[^0-9a-fA-F]/))
        throw new Error('Key should be 32 bytes in hexa');
      submitFunc(aesjs.utils.hex.toBytes(document.getElementById('key').value));
    } else {
      var hashObject = sha256.create();
      hashObject.update(document.getElementById('pass').value);
      submitFunc(new Uint8Array(hashObject.arrayBuffer()));
    }
  };
  var processButton = document.getElementById('processButton')
  processButton.textContent = operation;
  processButton.onclick = processFunc;
  var passInput = document.getElementById('pass');
  passInput.onkeyup = function(evt) {if (evt.key == 'Enter') processFunc();};
  document.getElementById('passPanel').style.display = "flex";
  document.getElementById('passPanelBack').style.display = "block";
  passInput.focus();
}
function hidePassPanel(){
  document.getElementById('passPanel').style.display = "none";
  document.getElementById('passPanelBack').style.display = "none";
  document.getElementById('showPass').checked = false;
  tooglePassVisibility();
}
function tooglePassVisibility() {
  var passInput = document.getElementById("pass");
  passInput.type = document.getElementById('showPass').checked ? "text" : "password";
}
function toogleKeyType() {
  var keyType = document.querySelector('input[name="keyType"]:checked').value;
  if (keyType == "key") {
    document.getElementById("passphasePanel").style.display = 'none';
    document.getElementById("keyPanel").style.display = 'block';
  } else {
    document.getElementById("keyPanel").style.display = 'none';
    document.getElementById("passphasePanel").style.display = 'block';
  }
}
function cipher(text, key) {
  if (key.length != 32) {
    throw new Error('unsupported key length');
  }
  const textBytes = aesjs.utils.utf8.toBytes(text);
  // textBytes must be a multiple of 16 bytes
  var paddingLength = 16 - (textBytes.length % 16);
  var bytesToCipher = null;
  if (paddingLength == 16) {
    paddingLength = 0;
    bytesToCipher = textBytes;
  } else {
    var paddingBytes = new Uint8Array(paddingLength);
    window.crypto.getRandomValues(paddingBytes);
    bytesToCipher = new Uint8Array(textBytes.length + paddingLength);
    bytesToCipher.set(textBytes);
    bytesToCipher.set(paddingBytes, textBytes.length);
  }
  var iv = new Uint8Array(16);
  window.crypto.getRandomValues(iv);
  var aesCbc = new aesjs.ModeOfOperation.cbc(key, iv);
  const encryptedText = aesCbc.encrypt(bytesToCipher);
  var encryptedData = new Uint8Array(encryptedText.length + 17);
  encryptedData[0] = paddingLength;
  encryptedData.set(iv, 1);
  encryptedData.set(encryptedText, 17);
  return encryptedData;
}
function decipher(data, key) {
  if (key.length != 32) {
    throw new Error('unsupported key length: ' + key.length);
  }
  if (data.length < 17) {
    throw new Error('unsupported data format');
  }
  const paddingLength = data[0];
  const iv = data.slice(1, 17);
  var aesCbc = new aesjs.ModeOfOperation.cbc(key, iv);
  const textBytes = aesCbc.decrypt(data.slice(17));
  if (paddingLength > 0)
    return aesjs.utils.utf8.fromBytes(textBytes.slice(0, -paddingLength));
  else
    return aesjs.utils.utf8.fromBytes(textBytes);
}
document.addEventListener("DOMContentLoaded", function(evt) {
  document.getElementById('showPass').checked = false;
  tooglePassVisibility();
  toogleKeyType();
});
