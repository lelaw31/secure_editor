# Secure Editor

Edit an AES-256 encrypted file in a browser. The encryption/description is done on the fly on client side. No password or encryption key is recorded or sent outside. The clear data are kept in volatile memory and just on the client.

## Planned update
Adding a online file editing with a section in the file containing instruction to update, protocol (openstack, S3, ...) and credentials. 
